#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   期貨資料
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.03.06
#   Purpose : go to http://www.tse.com.tw/ch/trading/exchange/MI_INDEX/MI_INDEX.php
#---------------------------------------------
"""

import urllib
import urllib2

def main():
    year = 2017
    month = 2
    day = 31
    [date, file_name]  = transformDate(year, month, day)

    url = "http://www.tse.com.tw/ch/trading/exchange/MI_INDEX/MI_INDEX.php"
    values = {'download' : 'csv',
              'qdate' : date,
              'selectType' : 'ALLBUT0999'}
    data = urllib.urlencode(values)
    req = urllib2.Request(url, data.encode('utf-8'))
    response = urllib2.urlopen(req)

    print(file_name)
    with open(file_name, 'wb') as f:
        f.write(response.read())


def transformDate(year, month, day):
    list = [year-1911, month, day]
    date = ""
    file_name = ""
    for item in list:
        ch = str(item)
        if len(ch) < 2:
            ch = "0" + ch
        file_name = file_name + ch
        date = date + ch + "/"
    file_name = file_name + ".csv"
    return [date, file_name]


if __name__ == "__main__":
    main()
