#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   主力進出
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.04.10
#   Purpose : go to http://jsjustweb.jihsun.com.tw/z/zc/zco/zco.djhtm?a=0050 download data
#---------------------------------------------
"""

import urllib, chardet, sys
from sgmllib import SGMLParser

# print(sys.getdefaultencoding())   #=ascii

class Parser(SGMLParser):
    def __init__(self):
        SGMLParser.__init__(self)

    def reset(self):
        SGMLParser.reset(self)
        self.start = False
        self.header = False
        self.item = False
        self.data = False
        self.rowcount = 0
        self.columncount = 0
        self.tablecolumncount = 0
        self.info = []

    def parse(self, data):
        self.feed(data)
        self.close()

    def start_tr(self, attrs):
        for name, value in attrs:
            if len(attrs) == 1 and name == 'id':
                if value == 'oScrollMenu':
                    self.start = True

    def start_td(self, attrs):
        if len(attrs) == 2:
            if attrs[1][0] == 'colspan' and self.tablecolumncount == 0:
                self.tablecolumncount = int(attrs[1][1])

        if self.start:
            for name, value in attrs:
                if len(attrs) == 1 and name == 'class':
                    if value == 't2':
                        self.header = True
                        if self.columncount == 0:
                            self.info.append([])
                    elif value == 't4t1':
                        self.item = True
                        self.info.append([])
                    elif value == 't3n1' or value == 't3r1':
                        self.data = True

    def handle_data(self, text):
        if self.header:
            #print "header = "+ text
            self.header = False
            self.info[self.rowcount].append(text.strip())
            self.columncount += 1
        elif self.item:
            #print "Item = "+ text
            self.item = False
            self.info[self.rowcount].append(text.strip())
            self.columncount += 1
        elif self.data:
            #print "Data = " + text
            self.data = False
            self.info[self.rowcount].append(text)
            self.columncount += 1
        if self.columncount == self.tablecolumncount and self.tablecolumncount != 0:
            self.columncount = 0
            self.rowcount += 1


def changeCalendar(input_str):
    temp_year = input_str.split('.')[0]
    temp_year = str(int(temp_year) + 1911)
    output_str = temp_year + '.' + input_str.split('.')[1]
    return output_str


def main(stock_id):
    url = "http://jsjustweb.jihsun.com.tw/z/zc/zco/zco.djhtm?a=" + stock_id
    webcode = urllib.urlopen(url)
    if webcode.code == 200:
        stock = Parser()
        webdata = webcode.read()
        mychar = chardet.detect(webdata)
        print("Web Encoding : " + mychar['encoding'])
        stock.parse(webdata)
        webcode.close()

    for i in range(0, len(stock.info)):
        for j in range(0, len(stock.info[i])):
            if j == 0:
                s = stock.info[i][j]
                print(s.decode('Big5'))
            else:
                if i == 0:
                    print(changeCalendar(stock.info[i][j]),)
                else:
                    print(stock.info[i][j],)
        print("\n")

if __name__ == "__main__":
    stock_id = '1101'
    main(stock_id)
