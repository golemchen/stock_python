#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from urllib.parse import urlencode
import httplib2, time
import requests
 

class get_Daily_TWSE(object):
    today= (time.strftime("%Y/%m/%d", time.localtime()) )
    filename = (time.strftime("%Y_%m_%d", time.localtime()))+'_TWSW.csv'
    
    taiwanesetodaytime = str((int(today[0:4])-1911))+today[4:]

    url = 'http://www.twse.com.tw/ch/trading/exchange/MI_INDEX/MI_INDEX.php'
    data = {'download': 'csv', 
            'qdate': taiwanesetodaytime, 
            'selectType': 'ALLBUT0999'}
    agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'
    
    httplib2.debuglevel = 1
    conn = httplib2.Http('.cache')
    headers = {'Content-type': 'application/x-www-form-urlencoded',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
               'User-Agent': agent}


    def run(self):
        resp, content = self.conn.request(self.url, 'POST', urlencode(self.data), self.headers)
    
        with open(self.filename, 'wb') as f:
            f.write(content)
            


class get_Daily_TWSE_3Itrade_Hedge(get_Daily_TWSE):
           
    headers = {"User-Agent":"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36"}

    fName1 = (time.strftime("%Y_%m_%d", time.localtime()))+"_TWSW__3Itrade_Hedge.csv"

    url1 = "http://www.twse.com.tw/ch/trading/fund/T86/T86.php"
    payload1 = {"download": "csv",
                "qdate": get_Daily_TWSE.taiwanesetodaytime,
                "select2": "ALLBUT0999",
                "sorting": "by_issue"}

    res1 = requests.post(url1, headers=headers, data=payload1, stream=True)

    def run(self):
        with open(self.fName1, 'wb') as f1:
            for chunk in self.res1.iter_content(1024):
                f1.write(chunk)