#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
import lxml, html5lib, time


class get_Daily_OTC(object):
    today= (time.strftime("%Y/%m/%d", time.localtime()) )
    filename = (time.strftime("%Y_%m_%d", time.localtime()))+'_OTC.csv'
    
    taiwanesetodaytime = str((int(today[0:4])-1911))+today[4:]
    print(taiwanesetodaytime)
    url='http://www.tpex.org.tw/web/stock/aftertrading/otc_quotes_no1430/stk_wn1430_print.php?l=zh-tw&d='+taiwanesetodaytime+'&se=EW'
    print(url)
    def run(self):
        data = pd.read_html(self.url)[0]
        
        data.to_csv(self.filename)    


class get_Daily_OTC_3Itrade_Hedge(object):
    today= (time.strftime("%Y/%m/%d", time.localtime()) )
    filename = (time.strftime("%Y_%m_%d", time.localtime()))+'_OTC_3Itrade_Hedge.csv'
    
    taiwanesetodaytime = str((int(today[0:4])-1911))+today[4:]
    url='http://www.tpex.org.tw/web/stock/3insti/daily_trade/3itrade_hedge_print.php?l=zh-tw&se=EW&t=D&d='+taiwanesetodaytime+'&s=0,asc'
    
    def run(self):
        data = pd.read_html(self.url)[0]
        data.to_csv(self.filename)    


