from getDaily_TWSE import get_Daily_TWSE, get_Daily_TWSE_3Itrade_Hedge
from getDaily_OTC import get_Daily_OTC, get_Daily_OTC_3Itrade_Hedge

get_Daily_TWSE.run(get_Daily_TWSE)
get_Daily_TWSE_3Itrade_Hedge.run(get_Daily_TWSE_3Itrade_Hedge)
get_Daily_OTC.run(get_Daily_OTC)
get_Daily_OTC_3Itrade_Hedge.run(get_Daily_OTC_3Itrade_Hedge)