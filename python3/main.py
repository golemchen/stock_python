#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   股票資料
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.07.21
#   Purpose : main
#---------------------------------------------
"""

from src.getDailyPrice import DailyPrice
from src.getDaily3institution import Daily3institution
from src.getDailyPER import DailyPER
from src.getMajor import MajorInfo
from src.getPriceAvg import PriceAverage
from src.General import General
import time

year, month, day = General().getDate(True, 2019, 3, 8)
isUpload = True

c_dailyPrice = DailyPrice(year, month, day)
c_dailyPrice.getTWSE()
c_dailyPrice.saveTWSE(isUpload)

c_daily3institution = Daily3institution(year, month, day)
c_daily3institution.getTWSE()
c_daily3institution.saveTWSE(isUpload)

c_dailyPER = DailyPER(year, month, day)
c_dailyPER.getTWSE()
c_dailyPER.saveTWSE(isUpload)

start = time.time()
ma_list = [1, 5, 10, 20, 60, 100]
c_majorInfo = MajorInfo(year, month, day)
c_majorInfo.processTWSE(isUpload)
c_priceAverage = PriceAverage(ma_list, year, month, day, True)
c_priceAverage.processTWSE(isUpload)
print('time : ', (time.time()-start))
