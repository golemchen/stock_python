#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   data processing
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.04.11
#   Purpose :
#---------------------------------------------
"""

import csv
from datetime import datetime

def main():
    year = datetime.now().strftime('%Y')
    year = str(int(year) - 1911)
    month = datetime.now().strftime('%m')
    day = datetime.now().strftime('%d')
    if int(datetime.now().strftime('%H')) < 8:
        day = str(int(day) - 1)
    file_name_out = "stockid_modified.csv"

    with open('stockid.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        stock_id_list = list(reader)[1:]

    index = 0
    for row in stock_id_list:
        stock_id = str(row[0])
        if stock_id.find('="') != -1:
            print(stock_id)
            stock_id = stock_id.strip('="').strip('"').rstrip()
            print(str(index) + " : " + str(stock_id))
            stock_id_list[index][0] = str(stock_id)
        index += 1

    with open(file_name_out, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['證券代號',''])
        for row in stock_id_list:
            writer.writerow(row)

def transformDate(year, month, day):
    list = [year, month, day]
    date = ""
    file_name = ""
    for item in list:
        ch = str(item)
        if len(ch) < 2:
            ch = "0" + ch
        file_name = file_name + ch
        date = date + ch + "-"
    # file_name = file_name + ".csv"
    return [date[:-1], file_name]


if __name__ == "__main__":
    main()