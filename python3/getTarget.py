#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.04.23
#   Purpose : check if target stock_id is in the list
#---------------------------------------------
"""

import csv

def sameTarget(my_list_name, check_file_name):
    stock_id_list = []
    my_list_name = my_list_name + '.csv'
    with open(my_list_name, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        stock_id_list_tmp = list(reader)[1:]
    for row in stock_id_list_tmp:
        stock_id_list.append(row[0])

    check_file_name = check_file_name + '.csv'
    with open(check_file_name, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        check_list = list(reader)

    print('match of ',check_file_name)
    for row in check_list:
        stock_id = row[0]
        if stock_id in stock_id_list:
            print(row)

def traceTarget(file_in_a, file_in_b):
    print('a')

if __name__ == "__main__":
    sameTarget('stockid_golem', '20170412_20MA')


    traceTarget('20170406_W20MA.csv')

