#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   tools Class
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.05.21
#   Purpose :
#---------------------------------------------
"""
from src.General import General
import csv
import os

import urllib
import urllib.request
import urllib.parse
import httplib2
from bs4 import BeautifulSoup


def update_stockid():
    with open('stockid.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        stock_id_list = list(reader)[0:]
    with open('stockid_temp.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        input_list = list(reader)[1:]
    index = 0
    for row in stock_id_list:
        name = ''
        stock_id_tmp = row[0]
        if stock_id_tmp.find('="') != -1:
            stock_id_tmp = stock_id_tmp.strip('="').strip('"').rstrip()
        for row_in in input_list:
            stock_id_tmp_2 = row_in[0]
            if stock_id_tmp_2.find('="') != -1:
                stock_id_tmp_2 = stock_id_tmp_2.strip('="').strip('"').rstrip()
            if stock_id_tmp == stock_id_tmp_2:
                name = str(row_in[1])
                name = name.rstrip(' ')
                break
        stock_id_list[index][1] = name
        index += 1
    print(stock_id_list)
    with open('stockid.csv', 'w') as f:
        writer = csv.writer(f)
        for row in stock_id_list:
            writer.writerow(row)

def update_price_avg():
    with open('stockid.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        id_list = list(reader)
    folder_path = "/Users/nothingfrank/PycharmProjects/stock/Data/MA/2017.04"
    dirList = os.listdir(folder_path)
    output_list = []
    for file in dirList:
        if file.find('.csv') != -1:
            file_path = folder_path + '/' + file
            print(file)
            with open(file_path, 'r') as f:
                reader = csv.reader(f, delimiter=',')
                input_list = list(reader)
            input_list[0][0] = 'MA' + input_list[0][0]
            with open(file_path, 'w') as f:
                writer = csv.writer(f)
                for row in input_list:
                    writer.writerow(row)

            for index in range(len(input_list)):
                row = input_list[index]
                if index > 0:
                    row.insert(1,id_list[index-1][1])
                output_list.append(row)
            with open(file_path, 'w') as f:
                writer = csv.writer(f)
                for row in output_list:
                    writer.writerow(row)

def updateSelect():
    path = '/Users/nothingfrank/PycharmProjects/stock/Data/MAselected'
    for root, dirs, files in os.walk(path):
        print(root, end=',')
        print(dirs, end=',')
        print(files)

        for file in files:
            file_in = root + '/' + file
            if file != '.DS_Store':
                with open(file_in, 'r') as f:
                    reader = csv.reader(f)
                    data_in_list = list(reader)

                if len(data_in_list[0]) != 1:
                    [year, month, day] = General().transformFileDate(file, True)
                    [date, file_date] = General().transformDate(year, month, day, '', True)
                    data_out_list = [['W20MA_' + date]]
                    title = ['證券代號', '證券名稱']
                    ma_list = [1, 5, 10, 20, 60, 100]
                    for element in ma_list:
                        title.append(str(element) + ' ma')
                    data_out_list.append(title)
                    data_out_list.extend(data_in_list)

                    file_out = root + '/' + file.split('_')[0] + '_W20MA.csv'
                    print(file_out)
                    with open(file_out, 'w') as f:
                        writer = csv.writer(f)
                        for row in data_out_list:
                            writer.writerow(row)
                    os.remove(file_in)

def seperate_stockid():
    with open('unseperated_stockid.csv', 'r') as f:
        reader = csv.reader(f, delimiter=',')
        data_in_list = list(reader)[1:]
    data_out_list = []
    number = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    for row in data_in_list:
        seperate_ch_index = 0
        print(row[0])
        length = len(row[0])
        print(length)
        for index, ch in enumerate(row[0]):
            if ch in number:
                seperate_ch_index = index
            else:
                break
        id = row[0][:seperate_ch_index+1]
        name = row[0][seperate_ch_index+2:]
        data_out_list.append([id, name])
    for row in data_out_list:
        print(row)
    with open('seperated_stockid.csv', 'w') as f:
        writer = csv.writer(f)
        for row in data_out_list:
            writer.writerow(row)

def updateSelected():
    ref_file = '/Users/nothingfrank/PycharmProjects/stock/Data/2017.08/20170814_TWSE.csv'
    target_file = '/Users/nothingfrank/PycharmProjects/stock/Data/S_cs/20170724_trackingLog.csv'
    type = target_file.split('/')[-1].split('_')[1]
    shift = 0
    with open(ref_file, 'r') as f:
        reader = csv.reader(f, delimiter=',')
        ref_list = list(reader)[2:]
    if type == 'trackingLog.csv':
        shift = 2
    try:
        with open(target_file, 'r') as f:
            reader = csv.reader(f, delimiter=',')
            target_list = list(reader)
    except:
        with open(target_file, 'r', encoding='utf-16') as f:
            reader = csv.reader(f, delimiter=',')
            target_list = list(reader)
    print(target_list)

    date = ref_file.split('/')[-1].split('_')[0]
    title_in_begin_flag = 0
    if len(target_list[0]) > 1:
        title_in_begin_flag = 1
    if title_in_begin_flag == 1:
        title = target_list[0]
    else:
        title = target_list[1]
    title.append(date)
    title.append('%')
    if title_in_begin_flag != 1:
        out_put_list = [target_list[0]]
        out_put_list.append(title)
    else:
        out_put_list = [title]

    for ref_row in ref_list:
        ref_id = ref_row[0].split('=')[1]
        for target_row in target_list:
            target_id = target_row[shift]
            if target_id.find('="') != -1:
                target_id = target_id.strip('="').strip('"').rstrip()
            if target_id == ref_id:
                ref_price = float(ref_row[8])
                target_price = float(target_row[shift+2])
                percent = round((ref_price - target_price)/target_price*100, 3)
                target_row.append(str(ref_price))
                target_row.append(str(percent))
                out_put_list.append(target_row)
    for row in out_put_list:
        print(row)
    with open(target_file, 'w') as f:
        writer = csv.writer(f)
        for row in out_put_list:
            writer.writerow(row)

def getEPS():
    # https://stock.wespai.com/p/7733
    url = "https://stock.wespai.com/p/7733"
    agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'
    httplib2.debuglevel = 0
    conn = httplib2.Http('.cache')
    headers = {'Content-type': 'application/x-www-form-urlencoded',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
               'User-Agent': agent}
    data = urllib.parse.urlencode({})
    resp, content = conn.request(url, 'POST', data.encode('utf-8'), headers)
    soup = BeautifulSoup(content, 'html.parser')


    eps_list = []
    thead = soup.find('thead')
    ths = thead.find_all('th')
    title = []
    for th in ths:
        temp = str(th).split('>')[1].split('<')[0]
        title.append(temp)
    eps_list.append(title)

    trs = soup.find_all('tr')
    for i, tr in enumerate(trs):
        tds = tr.find_all('td')
        row = []
        for index, td in enumerate(tds):
            start = 1
            end = 0
            if (index == 1) or (index == 14) or (index == 16):
                start = 2
                end = 0
            temp = str(td).split('>')[start].split('<')[end]
            if index >= 3:
                temp = float(temp)
            row.append(str(temp))
        if(i > 0):
            eps_list.append(row)

    year, month, day = General().getDate(True, 2017, 6, 17)
    [date_AC, file_date] = General().transformDate(year, month, day, '', True)
    file_name_out = str(file_date)[:-2] + '_eps.csv'
    file_out = General().saveToData(file_name_out)
    print(file_out)
    with open(file_out, 'w') as f:
        writer = csv.writer(f)
        for row in eps_list:
            writer.writerow(row)


def main():
    getEPS()

if __name__ == "__main__":
    main()