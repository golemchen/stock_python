#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   均線
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.03.27
#   Purpose : TWSE go to http://www.twse.com.tw/ch/trading/exchange/STOCK_DAY_AVG/STOCK_DAY_AVGMAIN.php
              TPEX go to http://www.tpex.org.tw/web/stock/aftertrading/daily_trading_info/st43_print.php?l=zh-tw&d=106/04&stkno=1259&s=0,asc,0
#---------------------------------------------
"""
from src.General import General
from src.stockUpload import StockUpload
import time
import csv
import os

MAX_MONTH = 12
MAX_TRY = 2
MA_START = 2


class PriceAverage(object):

    def __init__(self, _ma_list, _year, _month, _day, _is_today):
        self.ma_list = _ma_list
        self.year = _year
        self.month = _month
        self.day = _day
        self.isToday = _is_today
        [self.date_AC, self.file_date] = General().transformDate(_year, _month, _day, '', True)

    def processTWSE(self, isUpload):
        stock_id_list = General().get_TWSE_id_list()
        path = os.path.abspath(__file__)
        for i in range(2):
            path = os.path.split(path)[0]
        if self.isToday == True:
            with open(path + '/template/stock_price.csv', 'r') as f:
                reader = csv.reader(f, delimiter=',')
                stock_price_list = list(reader)
            stock_price_date = stock_price_list[0][0].split(' ')[-1]
            del stock_price_list[:2]

            with open(General().saveToData(self.file_date + "_TWSE.csv"), 'r') as f:
                reader = csv.reader(f, delimiter=',')
                stock_list_today = list(reader)
        # for i in stock_price_list:
        #     print(i)

        price_list = [['price till ' + self.date_AC]]
        price_avg_list = [['MA' + self.date_AC]]
        weekly_through_list = [['W20MA_' + self.date_AC]]
        title = ['證券代號', '證券名稱']
        price_list.append(title.copy())
        for element in self.ma_list:
            title.append(str(element) + ' ma')
        price_avg_list.append(title)
        weekly_through_list.append(title)

        for index in range(len(stock_id_list)):
            stock_id = stock_id_list[index][0]
            # print(stock_id)
            if self.isToday:
                price = self.searchSinglePriceFromTWSEFile(stock_id, stock_list_today)
                if price == -1:
                    print(stock_id + ' is not exist')
                    price = 0
                log_raw, log_price_raw = self.mergeSingleRawPrice(stock_id, price, stock_price_list[index][2:], self.ma_list[-1])
                # log_raw, log_price_raw = self.getSingleRawPriceFromFile(stock_id, self.year, self.month, self.day, stock_price_list[index][2:], stock_price_date, self.ma_list[-1])
            else:
                log_raw, log_price_raw = self.getSingleRawPrice(stock_id, self.year, self.month, self.day, self.ma_list[-1])

            if len(log_price_raw) > self.ma_list[-1]:
                del log_price_raw[self.ma_list[-1]:]
            # print(len(log_price_raw))

            log_ma = self.calculateMA(stock_id, self.ma_list, log_price_raw)
            log_ma.insert(1, stock_id_list[index][1])
            price_avg_list.append(log_ma)

            log_price_raw.insert(0, stock_id_list[index][0])
            log_price_raw.insert(1, stock_id_list[index][1])
            price_list.append(log_price_raw)

            # if self.stockSelection(log_ma, self.ma_list):
            #     weekly_through_list.append(log_ma)
            if (index % 50) == 0:
                print(str((index*100)//len(stock_id_list))+'%')

        file_name_out = path + '/template/stock_price.csv'
        print(file_name_out)
        with open(file_name_out, 'w') as f:
            writer = csv.writer(f)
            for row in price_list:
                writer.writerow(row)

        file_name_out = self.file_date + "_price_avg.csv"
        file_out = General().saveToData(file_name_out)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            for row in price_avg_list:
                writer.writerow(row)
        if isUpload:
            StockUpload().updload(file_out)

        print(len(weekly_through_list) - 2)
        for index, row in enumerate(weekly_through_list):
            if index > 2:
                print(row)

        file_name_out = self.file_date + "_W20MA.csv"
        file_out = General().saveToData(file_name_out)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            for row in weekly_through_list:
                writer.writerow(row)

    def getSingleMonthlyPrice(self, stock_id, year, month):
        stock_id_tmp = stock_id
        if stock_id_tmp.find('="') != -1:
            stock_id_tmp = stock_id_tmp.strip('="').strip('"').rstrip()
        if len(str(month)) < 2:
            month_tmp = '0' + str(month)
        else:
            month_tmp = str(month)
        date = str(year) + str(month_tmp) + '01'
        url = 'http://www.twse.com.tw/exchangeReport/STOCK_DAY_AVG?response=html&date=' + date + '&stockNo=' + stock_id_tmp
        soup = General().getFromPrintHTML(url, {})
        tbody = soup.find('tbody')
        if tbody == None:
            return []
        trs = tbody.find_all('tr')
        log, log_price, element = [], [], []
        for tr in trs:
            tds = tr.find_all('td')
            valid_flag = 0
            for td in tds:
                temp = str(td).split('>')[1].split('<')[0]
                if temp != '--':
                    element.append(temp)
                    valid_flag = 1
                else:
                    valid_flag = 0
            if valid_flag == 1:
                log.append(element)
                log_price.append(element[1])
            element = []
        log = log[:-1]
        log_price = log_price[:-1]
        return log, log_price

    def getSingleRawPrice(self, stock_id, year, month, day, days):
        log_raw, log_price_raw = [], []
        size_raw_data = 0
        year_tmp, month_tmp = year, month
        months = 0
        retry = 0
        next_month_flag = 0
        while size_raw_data < days:
            try:
                log, log_price = self.getSingleMonthlyPrice(stock_id, year_tmp, month_tmp)
                time.sleep(3)
                print(str(stock_id) + ', ' + str(year_tmp) + ', ' + str(month_tmp))
                if year_tmp == year and month_tmp == month:
                    same_date = self.removeDateAfter(log, year, month, day)
                    if same_date >= -1:
                        del log_price[same_date+1:]
                size_raw_data += len(log)
                log.reverse()
                log_price.reverse()
                log_raw = log_raw + log
                log_price_raw = log_price_raw + log_price
                next_month_flag = 1
                retry = 0
            except:
                print('delay 1 sec *', stock_id)
                time.sleep(1)
                retry += 1
                if retry >= MAX_TRY:
                    next_month_flag = 1
                    retry = 0
            finally:
                if next_month_flag == 1:
                    if month_tmp == 1:
                        month_tmp = 12
                        year_tmp -= 1
                    else:
                        month_tmp -= 1
                    months += 1
                    next_month_flag = 0
                if months > (MAX_MONTH//4) and size_raw_data < (days//3):
                    print(str(stock_id) + ', months = ' + str(months) + ', days = ', str(size_raw_data))
                    return log_raw, log_price_raw
                elif months > MAX_MONTH:
                    print(str(stock_id) + ', months = ' + str(months) + ', days = ', str(size_raw_data))
                    return log_raw, log_price_raw
        print(log_price_raw)
        return log_raw, log_price_raw


    def mergeSingleRawPrice(self, stock_id, price, prices, days):
        log_price_raw = prices.copy()
        log_price_raw.insert(0, price)
        if len(log_price_raw) >= days:
            del log_price_raw[days:]
        # print(stock_id + ':')
        # print(log_price_raw)
        return [], log_price_raw


    def searchSinglePriceFromTWSEFile(self, stock_id, stock_list):
        price = 0
        id =''
        stock_id_str = ''
        for line in stock_list:
            id = str(line[0]).split('=')[-1].strip(' ')
            if str(stock_id).find('=') == 0:
                stock_id_str = str(stock_id).split('="')[-1].split('"')[0].strip(' ')
            else:
                stock_id_str = str(stock_id)

            if stock_id_str == id:
                price = line[8]
                break
            else:
                price = -1

        return price


    def getSingleRawPriceFromFile(self, stock_id, year, month, day, prices, price_date, days):
        log_price_raw = prices.copy()
        retry = 0
        update_success = 0
        while (update_success == 0) and (retry<=3):
            try:
                log, log_price = self.getSingleMonthlyPrice(stock_id, year, month)
                time.sleep(3)
                target_year, target_month, target_day = General().transformFileDate(price_date, True)
                print(log)
                print(log_price)
                same_date = self.removeDateBefore(log, target_year, target_month, target_day)
                if same_date >= -1:
                    del log_price[:same_date + 1]
                new_data_size = len(log_price)
                if len(prices) >= days:
                    del log_price_raw[days - new_data_size:]
                for element in log_price:
                    log_price_raw.insert(0, element)
                print(stock_id + ':' + str(new_data_size))
                update_success = 1
            except:
                print(str(stock_id) + ' retry ' + str(retry) + " times")
                retry += 1
                time.sleep(1)
        if update_success == 0:
            print(stock_id + ' FAIL to get data')
        return [], log_price_raw

    def calculateMA(self, stock_id, ma_list, raw):
        log_ma = [stock_id]

        if len(raw) >= ma_list[2]:  # >= 20MA
            i, sum_raw = 0, 0
            for element in raw:
                try:
                    str_tmp = str(element).replace(',','')
                    sum_raw += float(str_tmp)
                    i += 1
                except:
                    print('except "calculate"')
                    print(stock_id)
                    print(raw)
                    sum_raw += 0
                index = 0
                for ma in ma_list:
                    if i == ma:
                        log_ma.append(round(sum_raw / ma, 3))
                    index += 1
        # print(log_ma)
        if len(log_ma)<len(ma_list)+1:
            for i in range(len(ma_list)+1-len(log_ma)):
                log_ma.append(0)
        return log_ma

    def removeDateAfter(self, log, year, month, day):
        [target_date, file_date] = General().transformDate(year, month, day, '', False)
        same_date = -2
        for index in range(len(log)-1):
            date = str(log[index][0])
            for ch in date:
                if ch == '/':
                    date = date.replace(ch,'')
            if date == target_date:
                same_date = index
                break
            elif date > target_date:
                same_date = index - 1
                break
        if same_date >= -1:
            del log[same_date+1:]
        return same_date

    def removeDateBefore(self, log, year, month, day):
        [target_date, file_date] = General().transformDate(year, month, day, '', False)
        same_date = -2
        for index in range(len(log)-1):
            date = str(log[index][0])
            for ch in date:
                if ch == '/':
                    date = date.replace(ch,'')
            if date == target_date:
                same_date = index
                break
            elif date > target_date:
                same_date = index - 1
                break
        if same_date >= -1:
            del log[:same_date+1]
        return same_date

    def stockSelection(self, log_ma, ma_list):
        result = False
        if len(log_ma) == (len(ma_list) + 2):  # with 100MA
            ma_1 = MA_START
            ma_5 = ma_1 + 1
            ma_10 = ma_5 + 1
            ma_100 = -1
            ma_60 = ma_100 - 1
            if log_ma[ma_1] > log_ma[ma_5] and log_ma[ma_5] > log_ma[ma_10] and log_ma[ma_5] > log_ma[ma_100]:  # 1MA > 5MA, 5MA > 10MA, 5MA > week20MA
                if (log_ma[ma_10] * 2 - log_ma[ma_5]) < log_ma[ma_100]:  # last 5MA < week20MA
                    if log_ma[ma_60] < log_ma[ma_100]:  # week12MA < week20MA
                        result = True
        return result


if __name__ == "__main__":
    ma_list = [1, 5, 10, 20, 60, 100]
    isTodayData = True
    start = time.time()
    year, month, day = General().getDate(True, 2018, 11, 9)
    c_priceAverage = PriceAverage(ma_list, year, month, day, isTodayData)
    c_priceAverage.processTWSE(True)
    print('time : ', (time.time() - start))

    # #----- check functions -----#
    # stock_id = '1101'
    # log_raw, log_price_raw = c_priceAverage.getSingleRawPrice(stock_id, year, month, day, ma_list[-1])
    # print(len(log_price_raw))
    # print(len(log_raw))
    # print('')
    # if len(log_price_raw) > ma_list[-1]:
    #     del log_price_raw[ma_list[-1]:]
    # print(len(log_price_raw))
    # print(log_price_raw)

    # print('')
    # log_ma = calculateMA(stock_id, ma_list, log_raw)
    # print(log_ma)
    # print('')
    # if len(log_ma) > 1:
    #     if log_ma[1] > log_ma[-1]:
    #         print(stock_id)

    #----- selection -----#
    # [date, file_date] = transformDate(year, month, day, '', True)
    # file_name = file_date + "_price_avg.csv"
    # with open(file_name, 'r') as f:
    #     reader = csv.reader(f, delimiter=',')
    #     stock_list = list(reader)[2:]
    # for row in stock_list:
    #     log_ma = []
    #     for index in range(len(row)):
    #         if index >= 2:
    #             log_ma.append(float(row[index]))
    #         else:
    #             log_ma.append(row[index])
    #     if stockSelection(log_ma, ma_list):
    #         print(log_ma)

    # stock_id = '2636'
    # getSingleRawPriceFromFile(stock_id, year, month, day, ['1','2','3'], '20170602')