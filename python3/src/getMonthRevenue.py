#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   均線
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.09.17
#   Purpose : TWSE go to http://jsjustweb.jihsun.com.tw/z/zc/zch/zch_2330.djhtm
              TPEX go to
#---------------------------------------------
"""
from src.General import General
import time
import csv
import re
from enum import IntEnum

ONE_YEAR = 12
SHORT_PERIOD = 1
MID_PERIOD = 3
LONG_PERIOD = 12


class MonthRevenue(object):

    def __init__(self, _year, _month, _day):
        self.year = _year
        self.month = _month
        self.day = _day
        [self.date_AC, self.file_date] = General().transformDate(_year, _month, _day, '', True)
        self.operate = 0
        self.short_period = SHORT_PERIOD
        self.middle_period = MID_PERIOD
        self.long_period = LONG_PERIOD

    def getMonthRevenue(self):
        stock_id_list = General().get_id_list()
        revenue_list = []
        title = ['證券代號', '證券名稱']
        basic_title = ['日期', '股價', '營收(M)', 'YOY']
        for index in range(len(stock_id_list)):
            stock_id = stock_id_list[index][0]
            element = self.getSingleRevenue(stock_id,stock_id_list[index])
            revenue_list.append(element)

            if (index % 50) == 0:
                print(str((index*100)//len(stock_id_list))+'%')
                time.sleep(1)
        tmp_len = (len(revenue_list[0]) - 2)//4
        for i in range(tmp_len):
            title += basic_title
        revenue_list.insert(0, title)

        file_name_out = str(self.file_date)[:-2] + "_revenue.csv"
        file_out = General().saveToData(file_name_out)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            for row in revenue_list:
                writer.writerow(row)

    def getMonthRevenueFromFile(self, name):
        file_path = General().saveToData(name)
        print(file_path)
        with open(file_path, 'r') as f:
            reader = csv.reader(f, delimiter=',')
            revenue_list = list(reader)[1:]
        return revenue_list

    def getSingleRevenue(self, _stock_id, log):
        url = 'http://jsjustweb.jihsun.com.tw/z/zc/zch/zch_' + str(_stock_id) + '.djhtm'
        soup = General().getFromPrintHTML(url, {})
        tmp = soup.find_all('table')
        # trs = tmp[0].find_all('tr')
        # print(trs[1])
        # for tr in trs:
        #     print('1131423532646235n3jn53jk5k325jnk32j5nk325njk32n6k34jn6k23n68g8f8f8f8f88f8k2463jn63')
        #     print(tr)
        revenue_list = []
        revenue_1D_list = log
        for row in tmp:
            if (str(row).find('GetBcdData')) > 0:
                a = ((str(row).split("GetBcdData('"))[1].split("')"))[0]
                column_size = 4
                raw_list = re.split(',| ',a)
                row_size = len(raw_list) // column_size
                for i in range(0, row_size):
                    monthly_element = []
                    for j in range(0, column_size):
                        index = i + j * row_size
                        element = raw_list[index]
                        monthly_element.append(element)
                        # revenue_1D_list.append(element)
                    revenue_list.append(monthly_element)
        # print(revenue_list)
        if revenue_list != []:
            revenue_list = self.reconstructRevenue(revenue_list)
        # else:
        #     print('  not found')

        for element in revenue_list:
            revenue_1D_list.extend(element)
        return revenue_1D_list

    def reconstructRevenue(self, revenue_list):
        history_list = []
        for i in range(ONE_YEAR):
            tmp = [[],[],[],[]]
            year = int(str(revenue_list[i][0]).split('/')[0])-1
            tmp[0] = str(year) + '/' + str(revenue_list[i][0]).split('/')[1] + '/01'
            b = (float(revenue_list[i][3])/100+1)
            if b != 0:
                tmp[2] = str(round(float(revenue_list[i][2]) / (float(revenue_list[i][3])/100+1), 2))
            else:
                tmp[2] = '0'
            history_list.append(tmp)
        history_list.extend(revenue_list)
        # for element in history_list:
        #     print(element)
        return history_list

    def calculateIncreasement(self, history, ID, algorithm_type, is_log):
        history_list = []
        _history = history.copy()
        while _history:
            history_list.append(_history[:4])
            _history = _history[4:]
        # print(history_list)

        inc_list_length = len(history_list) - 23
        increase_list = []
        rate_of_return = 0
        state = 0
        price_in = 9999
        date_in = 0
        index_in = 0
        for i in range(inc_list_length):
            tmp = [[],[],[],[],[]]
            tmp[e_data.date] = history_list[23+i][0]
            tmp[e_data.short] = round(self.calculateMovingAverage(self.short_period, history_list, i), 3)
            tmp[e_data.middle] = round(self.calculateMovingAverage(self.middle_period, history_list, i), 3)
            tmp[e_data.price] = history_list[23+i][1]
            tmp[e_data.long] = round(self.calculateMovingAverage(self.long_period, history_list, i), 3)
            increase_list.append(tmp)
            if algorithm_type == 1:
                if i >= 1:
                    if increase_list[i][e_data.short] > increase_list[i][e_data.middle] and\
                                    increase_list[i-1][e_data.short] < increase_list[i-1][e_data.middle]:
                        if state == 0:
                            state = 1
                    elif increase_list[i][e_data.short] < increase_list[i][e_data.middle]:
                        if state == 2:
                            state = 3
            elif algorithm_type == 2:
                if i >= 2:  # if use 3, win_rate = 0.5952; use 2, win_rate = 0.6258
                    if increase_list[i][e_data.short] - increase_list[i][e_data.middle] > 0 and \
                                    increase_list[i-1][e_data.short] - increase_list[i-1][e_data.middle] > 0 and \
                                    increase_list[i][e_data.short] - increase_list[i-1][e_data.short] > 0 and \
                                    increase_list[i-2][e_data.short] - increase_list[i-2][e_data.middle] < 0 :
                        if state == 0:
                            state = 1
                    elif (increase_list[i][e_data.short] < increase_list[i][e_data.middle] and \
                                    increase_list[i-1][e_data.short] > increase_list[i-1][e_data.middle]) or \
                                    i == inc_list_length-1:
                        if state == 2:
                            state = 3
            elif algorithm_type == 3:
                if i >= 2:
                    if increase_list[i][e_data.short] - increase_list[i][e_data.middle] > 0 and \
                                    increase_list[i-1][e_data.short] - increase_list[i-1][e_data.middle] > 0 and \
                                    increase_list[i][e_data.short] - increase_list[i-1][e_data.short] > 0 and \
                                    increase_list[i-2][e_data.short] - increase_list[i-2][e_data.middle] < 0 :
                        if state == 0:
                            state = 1
                    elif (increase_list[i][e_data.short] < increase_list[i][e_data.middle] and \
                                    increase_list[i-1][e_data.short] > increase_list[i-1][e_data.middle]) or \
                                    i == inc_list_length-1:
                        if state == 2:
                            state = 3
            if state == 1:
                state = 2
                price_in = float(increase_list[i][3])
                a = str(increase_list[i][0]).split('/')
                date_in = int(a[0]) * 12 + int(a[1])
                index_in = i
                if date_in == (self.year*12+self.month-1):
                    print('ID = ', ID[0], ID[1])
                    print('  buy : ' + str(increase_list[i][0]) + ', price = ' + str(increase_list[i][3]))
                    print('  s : ' + str(round(increase_list[i][1], 3)) + ', l = ' + str(round(increase_list[i][2], 3)))
                    print('  s-1 : ' + str(round(increase_list[i-1][1], 3)) + ', l-1 = ' + str(round(increase_list[i-1][2], 3)))
                    print('  s-2 : ' + str(round(increase_list[i-2][1], 3)) + ', l-2 = ' + str(round(increase_list[i -2][2], 3)))
            elif state == 3:
                state = 0
                if price_in != 0:
                    self.operate += price_in*0.001
                    a = str(increase_list[i][e_data.date]).split('/')
                    duration = int(a[0]) * ONE_YEAR + int(a[1]) - date_in
                    _rate_of_return = ((float(increase_list[i][e_data.price]) / price_in) - 1)
                    rate_of_return += (_rate_of_return*ONE_YEAR/duration)
                    # print('ID = ', ID[0], ID[1])
                    # print('  buy : ' + str(increase_list[index_in][0]) + ', price = ' + str(increase_list[index_in][3]))
                    # print('  sell : ' + str(increase_list[i][0]) + ', price = ' + str(increase_list[i][3]) + \
                    #       ', rate_of_return = ' + str(round(_rate_of_return, 5)) + ', duration = ' + str(duration))


        if is_log:
            for element in increase_list:
                print(element)
        # print('  [rate of return] = ', round(rate_of_return, 5))
        return rate_of_return

    def calculateMovingAverage(self, size, list, current_index):
        a, b = 0, 0
        tmp = 0
        for j in range(size):
            a += float(list[23 + current_index - j][2])
            b += float(list[23 + current_index- j - ONE_YEAR][2])
        if b != 0:
            tmp = (a / b - 1) * 100
        else:
            tmp = -100
        return tmp

    def reInit(self, _short, _middle, _long):
        self.short_period = _short
        self.middle_period = _middle
        self.long_period = _long
        self.operate = 0


class e_data(IntEnum):
    # date = 0
    # short = 1
    # middle = 2
    # price = 3
    # long = 4
    date = 0
    short = 2
    middle = 3
    price = 1
    long = 4


if __name__ == "__main__":
    year, month, day = General().getDate(True, 2017, 6, 17)
    start = time.time()
    c_monthRevenue = MonthRevenue(year, month, day)
    sel_type = 3
    if(sel_type==1):
        c_monthRevenue.getMonthRevenue()
    elif(sel_type==2):
        revenue_list = c_monthRevenue.getMonthRevenueFromFile('201807_revenue.csv')
        total_rate = 0
        positive = 0
        negative = 0
        for element in revenue_list:
            rate = c_monthRevenue.calculateIncreasement(element[2:], element[:2], 2, False)
            total_rate += rate
            if rate > 0:
                positive += 1
            elif rate < 0:
                negative += 1
        if positive!=0 and negative!=0:
            win_rate = round(float(positive/(positive+negative)), 5)
        else:
            win_rate = -1
        print('total operation = ', round(c_monthRevenue.operate, 2))
        print('total rate of return = ', round(total_rate, 5))
        print('win rate = ', win_rate)
        print('res = ', round(total_rate/c_monthRevenue.operate, 3))

    elif(sel_type==3):
        a = c_monthRevenue.getSingleRevenue(5871, [])
        c_monthRevenue.calculateIncreasement(a, [1,1], 2, True)
    elif(sel_type==4):
        res = [[]]
        revenue_list = c_monthRevenue.getMonthRevenueFromFile('201806_revenue.csv')
        for i in range(1,2):
            for j in range(12, 13):
                # for k in range(i+1, j+1):
                k = 12
                if(k==12):
                    tmp = []
                    tmp.append(i)
                    tmp.append(j)
                    tmp.append(k)
                    c_monthRevenue.reInit(i,j,k)
                    total_rate = 0
                    positive = 0
                    negative = 0
                    operation_money = 0
                    for element in revenue_list:
                        rate = c_monthRevenue.calculateIncreasement(element[2:], element[:2], 3, False)
                        total_rate += rate
                        if rate > 0:
                            positive += 1
                        elif rate < 0:
                            negative += 1
                    if positive != 0 and negative != 0:
                        win_rate = round(float(positive / (positive + negative)), 5)
                    else:
                        win_rate = -1
                    total_rate = round(total_rate, 3)
                    operation_money = round(c_monthRevenue.operate, 3)

                    # print('total operation = ', operation_money)
                    # print('total rate of return = ', total_rate)
                    # print('positive = ', positive, ', negative = ', negative, ', win rate = ', win_rate)
                    tmp.append(operation_money)
                    tmp.append(total_rate)
                    tmp.append(win_rate)
                    tmp.append(round(total_rate/operation_money, 3))
                    res.append(tmp)
                    print(tmp)


    print('time : ', (time.time() - start))


