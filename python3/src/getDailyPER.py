#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   股票資料
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.07.21
#   Purpose : daily
#---------------------------------------------
"""

from src.General import General
from src.stockUpload import StockUpload
import csv


class DailyPER(object):
    twse_per_list = []

    def __init__(self, _year, _month, _day):
        self.year = _year
        self.month = _month
        self.day = _day
        [self.date_AC, self.file_date] = General().transformDate(_year, _month, _day, '', True)

    def getTWSE(self):    # TWSE : 本益比、殖利率、股價淨值比
        url = 'http://www.tse.com.tw/exchangeReport/BWIBBU_d?response=html&date=' + self.date_AC + '&selectType=ALL'
        # url = 'http://www.tse.com.tw/fund/T86?response=html&date=' + self.date_AC + '&selectType=ALL'
        soup = General().getFromPrintHTML(url, {})
        thead = soup.find('thead')
        div = thead.find('div')
        title = str(div).split('>')[1].split('<')[0]
        old_year = title.split('年')[0]
        if int(old_year) < 1911:
            title = str(self.year) + title.split(old_year)[1]
        trs = thead.find_all('tr')
        tds = trs[1].find_all('td')
        item_list = []
        for td in tds:
            temp = str(td).split('>')[1].split('<')[0]
            item_list.append(temp)

        self.twse_per_list = [[title]]
        self.twse_per_list.append(item_list)
        tbody = soup.find('tbody')
        trs = tbody.find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            element = []
            for index, td in enumerate(tds):
                temp = str(td).split('>')[1].split('<')[0]
                if index == 0:
                    temp = '=' + temp.strip()
                elif index == 1:
                    temp = temp.strip()
                else:
                    if temp == '' or temp == ' ' or temp == '--':
                        temp = '0'
                    temp = temp.replace(',', '')
                element.append(temp)
            self.twse_per_list.append(element)
        # for row in twse_per_list:
        #     print(row)

    def saveTWSE(self, isUpload):
        file_name = self.file_date + "_TWSE_PER.csv"
        file_out = General().saveToData(file_name)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            for row in self.twse_per_list:
                writer.writerow(row)
        if isUpload:
            StockUpload().updload(file_out)

if __name__ == "__main__":
    year, month, day = General().getDate(True, 2018, 12, 13)
    c_dailyPER = DailyPER(year, month, day)
    c_dailyPER.getTWSE()
    c_dailyPER.saveTWSE(True)

