#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   股票資料
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.07.21
#   Purpose : daily
#---------------------------------------------
"""

from src.General import General
from src.stockUpload import StockUpload
import csv


class DailyPrice(object):
    twse_price_list = []

    def __init__(self, _year, _month, _day):
        self.year = _year
        self.month = _month
        self.day = _day
        [self.date_AC, self.file_date] = General().transformDate(_year, _month, _day, '', True)

    def getTWSE(self):
        url = 'http://www.tse.com.tw/exchangeReport/MI_INDEX?response=html&date=' + self.date_AC + '&type=ALLBUT0999'
        soup = General().getFromPrintHTML(url, {})
        theads = soup.find_all('thead')
        trs = theads[-1].find_all('tr')
        title = str(trs[0].find('th')).split('>')[1].split('<')[0]
        old_year = title.split('年')[0]
        if int(old_year) < 1911:
            title = str(self.year) + title.split(old_year)[1]
        self.twse_price_list = [[title]]
        tds = trs[2].find_all('td')
        item_list = []
        for td in tds:
            temp = str(td).split('>')[1].split('<')[0]
            item_list.append(temp)
        self.twse_price_list.append(item_list)
        tbodys = soup.find_all('tbody')
        trs = tbodys[-1].find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            element = []
            for index, td in enumerate(tds):
                column = 1
                if index == 9:
                    column = 2
                temp = str(td).split('>')[column].split('<')[0]
                if index == 0:
                    temp = '=' + temp
                elif index == 9:
                    if temp == '+':
                        temp = '1'
                    elif temp == '-':
                        temp = '-1'
                    else:
                        temp = '0'
                else:
                    if temp == '' or temp == ' ' or temp == '--':
                        temp = '0'
                    temp = temp.replace(',', '')
                element.append(temp)
            self.twse_price_list.append(element)
        # for row in twse_price_list:
        #     print(row)

    def getTWSE2(self):
        for type in range(1,20):
            if(type<10):
                type_s = '0' + str(type)
            else:
                type_s = str(type)

            url = 'http://www.tse.com.tw/exchangeReport/MI_INDEX?response=html&date=' + self.date_AC + '&type=' + type_s
            print(url)
            soup = General().getFromPrintHTML(url, {})
            theads = soup.find_all('thead')
            trs = theads[0].find_all('tr')
            title = str(trs[0].find('th')).split('>')[1].split('<')[0]
            old_year = title.split('年')[0]
            if int(old_year) < 1911:
                title = str(self.year) + title.split(old_year)[1]
            if type == 1:
                self.twse_price_list = [[title]]
            tds = trs[2].find_all('td')
            item_list = []
            for td in tds:
                temp = str(td).split('>')[1].split('<')[0]
                item_list.append(temp)
            if type == 1:
                self.twse_price_list.append(item_list)
            tbodys = soup.find_all('tbody')
            trs = tbodys[-1].find_all('tr')
            for tr in trs:
                tds = tr.find_all('td')
                element = []
                for index, td in enumerate(tds):
                    column = 1
                    if index == 9:
                        column = 2
                    temp = str(td).split('>')[column].split('<')[0]
                    if index == 0:
                        temp = '=' + temp
                    elif index == 9:
                        if temp == '+':
                            temp = '1'
                        elif temp == '-':
                            temp = '-1'
                        else:
                            temp = '0'
                    else:
                        if temp == '' or temp == ' ' or temp == '--':
                            temp = '0'
                        temp = temp.replace(',', '')
                    element.append(temp)
                self.twse_price_list.append(element)
            for row in self.twse_price_list:
                print(row)

    def saveTWSE(self, isUpload):
        file_name = self.file_date + "_TWSE.csv"
        file_out = General().saveToData(file_name)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            for row in self.twse_price_list:
                writer.writerow(row)
        if isUpload:
            StockUpload().updload(file_out)

if __name__ == "__main__":
    year, month, day = General().getDate(False, 2018, 6, 15)
    c_dailyPrice = DailyPrice(year, month, day)
    c_dailyPrice.getTWSE()
    c_dailyPrice.saveTWSE(False)
