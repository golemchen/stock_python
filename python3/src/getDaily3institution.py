#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   股票資料
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.07.21
#   Purpose : daily
#---------------------------------------------
"""

from src.General import General
from src.stockUpload import StockUpload
import csv


class Daily3institution(object):
    twse_3insti_list = []

    def __init__(self, _year, _month, _day):
        self.year = _year
        self.month = _month
        self.day = _day
        [self.date_AC, self.file_date] = General().transformDate(_year, _month, _day, '', True)

    def getTWSE(self):
        url = 'http://www.twse.com.tw/fund/T86?response=html&date=' + self.date_AC + '&selectType=ALLBUT0999'
        soup = General().getFromPrintHTML(url, {})
        content = soup.getText
        all_rows = str(content).split('<td>')
        title = ''
        item_list = []
        for index, row in enumerate(all_rows):
            if index == 0:
                title = row.split('<div>')[2].split('<')[0]
                old_year = title.split('年')[0]
                if int(old_year) < 1911:
                    title = str(self.year) + title.split(old_year)[1]
            else:
                if '<tbody>' in row:
                    break
                if row[0] != '<' or row[0] != '':
                    temp = row.split('<')[0] + row.split('>')[-1]
                    item_list.append(temp[:-1])

        self.twse_3insti_list = [[title]]
        self.twse_3insti_list.append(item_list)
        tbody = soup.find('tbody')
        trs = tbody.find_all('tr')
        for tr in trs:
            tds = tr.find_all('td')
            element = []
            for index, td in enumerate(tds):
                temp = str(td).split('>')[1].split('<')[0]
                if index == 0:
                    temp = '=' + temp.strip()
                elif index == 1:
                    temp = temp.strip()
                else:
                    if temp == '' or temp == ' ' or temp == '--':
                        temp = '0'
                    temp = temp.replace(',', '')
                element.append(temp)
            self.twse_3insti_list.append(element)
        # for row in twse_3insti_list:
        #     print(row)

    def saveTWSE(self, isUpload):
        file_name = self.file_date + "_TWSE_3insti.csv"
        file_out = General().saveToData(file_name)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            for row in self.twse_3insti_list:
                writer.writerow(row)
        if isUpload:
            StockUpload().updload(file_out)


if __name__ == "__main__":
    year, month, day = General().getDate(True, 2018, 11, 8)
    c_daily3institution = Daily3institution(year, month, day)
    c_daily3institution.getTWSE()
    c_daily3institution.saveTWSE(True)

