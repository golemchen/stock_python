#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   upload
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2018.01.07
#   Purpose : upload file to google drive
#---------------------------------------------
"""

from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient.http import MediaFileUpload

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/drive-python-quickstart.json
# SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly'    #rm ~/.credentials/drive-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/drive.file'
CLIENT_SECRET_FILE = 'client_secret_stockUpload.json'
APPLICATION_NAME = 'StockUpload'

class StockUpload:

    def get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, 'drive-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else: # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials


    def updload(self, _file):
        """Shows basic usage of the Google Drive API.

        Creates a Google Drive API service object and outputs the names and IDs
        for up to 10 files.
        """
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v3', http=http)

        # results = service.files().list(
        #     pageSize=10,fields="nextPageToken, files(id, name)").execute()
        # items = results.get('files', [])
        # if not items:
        #     print('No files found.')
        # else:
        #     print('Files:')
        #     for item in items:
        #         print('{0} ({1})'.format(item['name'], item['id']))

        file = str(_file)
        file_name = file.split('/')[-1]
        print(file_name)
        mime_type = 'text/csv'
        # folder_id = '1Tg3v_DZBTQzHY2RJLCtZ7EB6VF0ww0zY' # 2018.01
        # folder_id = '1J1Kl72eboO3aHcjAJ_PETk21f4TVpH1C' #2018.02
        # folder_id = '1CXTWGprFDav5aKg_Tu2R4LAnHGUO9-C5' # 2018.03
        # folder_id = '1zeCqNx4B7dUPzc9_DDeIZ0o0aGXeOCaX' # 2018.04
        # folder_id = '1LVDYss48Mr3jUp4A_5i4i75xjFYcus_V'  # 2018.05
        # folder_id = '1ok3ieLhoul_ODmu8al_IW2xMzPld7NL9' # 2018.06
        # folder_id = '18Oi4isdjGlOwrlAujKHYmIQsaQbwTKmW' # 2018.07
        # folder_id = '1cSn9TlEZYqRVp48toW9Nx0214LcLAsB3' # 2018.08
        # folder_id = '11WPJ0h-mawnyPdpeBXFUq42Z_vbsJ0VQ'  # 2018.09
        # folder_id = '1N3n54p5_T30vz30Xne5TIai1lw5itNE0' #2018.10
        folder_id = '1cmQEsBdI7dEY7vmTjcZWldxe0yig8Asi'  # 2018.12

        file_metadata = {'name': file_name}
        media = MediaFileUpload(file,
                                mimetype=mime_type)
        file = service.files().create(body=file_metadata,
                                      media_body=media,
                                      fields='id').execute()
        file_ID = file.get('id')
        print(file_ID)

        # Retrieve the existing parents to remove
        file = service.files().get(fileId=file_ID,
                                   fields='parents').execute()
        previous_parents = ",".join(file.get('parents'))
        # Move the file to the new folder
        file = service.files().update(fileId=file_ID,
                                      addParents=folder_id,
                                      removeParents=previous_parents,
                                      fields='id, parents').execute()

if __name__ == '__main__':
    path = '/Users/nothingfrank/PycharmProjects/stock/Data/201711_revenue.csv'
    c_stockUpload = StockUpload()
    c_stockUpload.updload(path)
