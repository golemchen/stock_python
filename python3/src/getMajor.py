#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   主力進出
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.04.10
#   Purpose : go to https://tw.stock.yahoo.com/d/s/major_" + stock_id + ".html
#---------------------------------------------
"""
from src.General import General
from src.stockUpload import StockUpload
import csv
import os
import time
MIN_NUM = 5
import urllib
import urllib.request
import urllib.parse

class MajorInfo(object):

    def __init__(self, _year, _month, _day):
        self.year = _year
        self.month = _month
        self.day = _day
        self.try_num = 0
        [self.date_AC, self.file_date] = General().transformDate(_year, _month, _day, '', True)

    def processTWSE(self, isUpload):
        file_name_out = self.file_date + "_major.csv"
        stock_id_list = General().get_TWSE_id_list()
        major_list = [['主力' + self.date_AC]]
        title = ['證券代號', '主力進出>=' + str(MIN_NUM)]
        major_list.append(title)
        for index, row in enumerate(stock_id_list):
            # get stock_id
            stock_id = row[0]
            if stock_id.find('="') != -1:
                stock_id = stock_id.strip('="').strip('"').rstrip()

            res = False
            while res == False:
                res, major_num = self.getSingleMajor(stock_id)

            major_list.append([stock_id_list[index][0], stock_id_list[index][1], major_num])
            stock_id_list[index][1] = major_num
            if (index % 20) == 0:
                time.sleep(0.1)
            if (index % 100) == 0:
                print(str((index*100)//len(stock_id_list))+'%')
                time.sleep(1)

        # print(stock_id_list)

        file_out = General().saveToData(file_name_out)
        print(file_out)
        with open(file_out, 'w') as f:
            writer = csv.writer(f)
            self.date_AC = '主力' + self.date_AC
            writer.writerow([self.date_AC, ''])
            writer.writerow(title)
            for row in stock_id_list:
                writer.writerow(row)
        if isUpload:
            StockUpload().updload(file_out)

        # for row in major_list:
        #     print(row)

    def getSingleMajor(self, stock_id):
        res = False
        try:
            url = "https://tw.stock.yahoo.com/d/s/major_" + stock_id + ".html"
            values = {'stock_id':stock_id}
            soup = General().getFromPrintHTML(url, values)
            table = soup.find_all('table')

        except:
            print(str(stock_id) + ' except of url')
            self.try_num += 1
            if(self.try_num>3):
                res = True
                self.try_num = 0
            return res, 0

        try:
            # tds = (table[7].find_all('td'))[12:]
            # print(table[1])
            tds = (table[1].find_all('td'))[12:]
            e_num, total = 0, 0
            log, element = [], []
            for td in tds:
                try:
                    a = ((str(td).split('class="ttt">'))[1].split('</td>')[0]).strip()
                    element.append(a)
                    e_num += 1
                    if e_num % 4 == 0:
                        if a != '':
                            int_a = int(a)
                            if abs(int_a) >= MIN_NUM:
                                # print(int_a)
                                total += int_a
                        log.append(element)
                        element = []
                except:
                    total = 0
                    print(str(stock_id) + 'except of "for td in tds" : ', td)
                    # print(log)
        except:  # can't find right table, i.e. no data for this stock_id
            total = 'N/A'
        print(str(stock_id) + ' : ' + str(total))
        res = True
        return res, total

if __name__ == "__main__":
    year, month, day = General().getDate(True, 2018, 12, 5)
    start = time.time()
    c_majorInfo = MajorInfo(year, month, day)
    c_majorInfo.processTWSE(True)
    # c_majorInfo.getSingleMajor('1729')
    print('time : ', (time.time() - start))
