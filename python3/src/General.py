#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#---------------------------------------------
#   General Class
#   Version : 1.0
#   Author : nothingfrank
#   Create Date : 2017.04.20
#   Purpose :
#---------------------------------------------
"""

from datetime import datetime, timedelta
import urllib
import urllib.request
import urllib.parse
import csv
import os
import httplib2
import random
from bs4 import BeautifulSoup

OPEN_HOUR = 9


class General:

    def transformDate(self, year, month, day, char, isDateInAC):
        list = [year, month, day]
        for index in range(len(list)):
            ch = str(list[index])
            if len(ch) < 2:
                list[index] = '0' + ch
            else:
                list[index] = ch
        file_date = list[0] + list[1] + list[2]
        if isDateInAC != True:
            list[0] = str(int(list[0]) - 1911)
        date = list[0] + char + list[1] + char + list[2]
        return [date, file_date]


    def transformFileDate(self, file_name, isDateInAC):
        date = (file_name.split('_'))[0]
        if isDateInAC:
            year = date[0:4]
            month = date[4:6]
            day = date[6:8]
        else:
            shift = 0
            if len(date) == 7:
                shift = 1
            year = date[0:(2+shift)]
            month = date[(2+shift):(4+shift)]
            day = date[(4+shift):(6+shift)]
        return [year, month, day]


    def get_TWSE_id_list(self):
        path = os.path.abspath(__file__)
        for i in range(2):
            path = os.path.split(path)[0]
        with open(path + '/template/stockid.csv', 'r') as f:
            reader = csv.reader(f, delimiter=',')
            stock_id_list = list(reader)[1:]
        return stock_id_list

    def get_id_list(self):
        path = os.path.abspath(__file__)
        for i in range(2):
            path = os.path.split(path)[0]
        with open(path + '/template/stockid_all.csv', 'r') as f:
            reader = csv.reader(f, delimiter=',')
            stock_id_list = list(reader)[1:]
        return stock_id_list

    def getFromPHP(self, url, values, file_name, save):
        # <!DOCTYPE HTML>
        data = urllib.parse.urlencode(values)
        req = urllib.request.Request(url, data.encode('utf-8'))
        response = urllib.request.urlopen(req)
        if save == True:
            print(file_name)
            with open(file_name, 'wb') as f:
                f.write(response.read())
        else:
            return response

    def getFromPrintHTML(self, url, values):
        agent1 = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'
        agent2 = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
        httplib2.debuglevel = 0
        if random.randint(0, 1) == 1:
            agent = agent1
        else:
            agent = agent2
        conn = httplib2.Http('.cache')
        headers = {'Content-type': 'application/x-www-form-urlencoded',
                   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                   'User-Agent': agent}
        data = urllib.parse.urlencode(values)
        resp, content = conn.request(url, 'POST', data.encode('utf-8'), headers)
        soup = BeautifulSoup(content, 'html.parser')
        return soup

    def getFromPrintHTML_tmp(self, url, values):
        agent1 = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0'
        agent2 = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
        httplib2.debuglevel = 0
        if random.randint(0, 1) == 1:
            agent = agent1
        else:
            agent = agent2
        conn = httplib2.Http('.cache')
        headers = {'Content-type': 'application/x-www-form-urlencoded',
                   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                   'User-Agent': agent}
        data = urllib.parse.urlencode(values)
        print(data)
        resp, content = conn.request(url)
        print(resp)
        print(content)
        soup = BeautifulSoup(content, 'html.parser')
        print(soup)
        return soup

    def getDate(self, isToday, year, month, day):
        if isToday:
            year = int(datetime.now().strftime('%Y'))
            month = int(datetime.now().strftime('%m'))
            day = int(datetime.now().strftime('%d'))
            if int(datetime.now().strftime('%H')) < OPEN_HOUR:
                yesterday = datetime.now() - timedelta(days=1)
                month = int(yesterday.strftime('%m'))
                day = int(yesterday.strftime('%d'))

        return year, month, day

    def saveToData(self, file_name):
        path = os.path.abspath(__file__)
        for i in range(3):
            path = os.path.split(path)[0]
        file_name_modified = path + '/Data/' + file_name
        return file_name_modified

    def saveResToData(self, file_dir, file_name, res):
        file_out = file_dir + file_name
        print(file_out)
        with open(file_out, 'wb') as f:
            f.write(res.read())

    def read_from_file(self, file_dir, file_name, save, is_price):
        file_name_in = file_name
        [year, month, day] = self.transformFileDate(file_name_in, True)
        date_AC = year + '年' + month + '月' + day + '日'
        date = str(int(year) - 1911) + '年' + month + '月' + day + '日'
        file_in = file_dir + file_name
        data = [[date_AC]]
        with open(file_in, 'r', encoding='cp950') as f:
            reader = csv.reader(f)
            log_flag = 0

            for index, row in enumerate(reader):
                change_flag = 0
                if log_flag == 1:
                    data.append(row)
                try:
                    if is_price == True:
                        if date_AC == row[0]:
                            log_flag = 1
                        # if date == row[0]:
                        #     log_flag = 1
                    else:
                        if date in row[0]:
                            log_flag = 1
                            change_flag = 1
                        # if date_AC in row[0]:
                        #     log_flag = 1
                        #     change_flag = 1
                    if change_flag == 1:
                        if date != row[0]:
                            data[0][0] = date_AC
                            data.append([row[0].split(date)[1].strip()])
                            change_flag = 0
                except:
                    change_flag = 0
        delete_file = save
        if delete_file == True:
            os.remove(file_in)

        file_out = file_in.split('.')[0] + '_M.csv'
        if save:
            print(file_out)
            with open(file_out, 'w') as f:
                writer = csv.writer(f)
                for row in data:
                    writer.writerow(row)
        else:
            return file_out, data


    def main(self):
        today = True
        year, month, day = self.getDate(today, 2017, 4, 12)
        [date, file_date] = self.transformDate(year, month, day, '-', True)
        print(date)
        print(file_date)

        [a, b, c] = self.transformFileDate('990203_123.csv', False)
        print('a={},b={},c={}'.format(a, b, c))

        parent_path = os.path.abspath(os.pardir) + '/Data/'
        print(parent_path)

if __name__ == "__main__":
    c_general = General()
    c_general.main()
